<?xml version="1.0" encoding="UTF-8" ?>
<!--
  ~ Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="neatlogic.module.rdm.dao.mapper.DashboardMapper">
    <select id="searchDashboardCount" parameterType="neatlogic.framework.rdm.dto.DashboardVo" resultType="int">
        SELECT
        count(DISTINCT a.`id`)
        FROM
        `rdm_dashboard` a
        <include refid="searchDashboardCondition"/>
    </select>

    <select id="getDashboardById" parameterType="java.lang.Long"
            resultType="neatlogic.framework.rdm.dto.DashboardVo">
        SELECT a.`id`,
               a.`app_id`      as appId,
               a.`name`,
               a.`description`,
               a.`is_active`   AS isActive,
               a.`fcu`,
               a.`fcd`,
               a.`lcu`,
               a.`lcd`,
               a.`widget_list` AS widgetListStr
        FROM `rdm_dashboard` a
        where id = #{value}
    </select>

    <select id="searchDashboard" parameterType="neatlogic.framework.rdm.dto.DashboardVo"
            resultType="neatlogic.framework.rdm.dto.DashboardVo">
        SELECT
        a.`id`,
        a.`app_id` as appId,
        a.`name`,
        a.`description`,
        a.`is_active` AS isActive,
        a.`fcu`,
        a.`fcd`,
        a.`lcu`,
        a.`lcd`,
        a.`widget_list` AS widgetListStr
        FROM
        `rdm_dashboard` a
        <include refid="searchDashboardCondition"/>
        ORDER BY a.id DESC
        limit #{startNum}, #{pageSize}
    </select>

    <sql id="searchDashboardCondition">
        <where>
            a.app_id = #{appId}
            <if test="keyword != null and keyword != ''">
                and a.name LIKE concat('%', #{keyword}, '%')
            </if>
            <if test="isActive != null">
                and a.is_active = #{isActive}
            </if>
        </where>
    </sql>

    <update id="updateDashboard" parameterType="neatlogic.framework.rdm.dto.DashboardVo">
        update rdm_dashboard
        set name        = #{name},
            description = #{description},
            widget_list = #{widgetListStr,typeHandler=CompressHandler},
            lcd         = now(),
            lcu         = #{lcu}
        where id = #{id}
    </update>

    <insert id="insertDashboard" parameterType="neatlogic.framework.rdm.dto.DashboardVo">
        insert into rdm_dashboard
        (id,
         app_id,
         name,
         description,
         fcd,
         fcu,
         widget_list)
        values (#{id},
                #{appId},
                #{name},
                #{description},
                now(),
                #{fcu},
                #{widgetListStr,typeHandler=CompressHandler})
    </insert>

    <delete id="deleteDashboardById" parameterType="java.lang.Long">
        delete
        from rdm_dashboard
        where id = #{value}
    </delete>
</mapper>
